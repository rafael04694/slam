# SLAM

This repo contains the code for a SLAM project I developed
The project consists in initially localizing a moving robot using odometry with addition to measurements of range/bearning to known landmarks

The base algorithm used in each task is the UKF, which had to be progressively improved to achieve good performance in each task.
The UKF is the most complex type of kalman filter, and its suitable for non-linear problems. EKF is also used in non linear problems, however, it relies on linearization around a operating point, which may make it slow and lead to unstability issues.

Here are the tasks that the algorithm had to be able to handle:

## Task 1
Consider that there are two landmaks located at (0, 0 and (10, 0), and that the initial pose of the robot
is known. Design and implement an algorihtm to estimate the position and orientation of the robot
based on the measurements of linear and angular velocities and also on the distances and bearings to
the two beacons. Use the data in file data1.txt to test the algorithm. 

## Task 2
Now assume that laser system has a finite field of view. When each landmark is not within the field of
view (±⇡4 ) the range measurement returns 0. Adapt the previous algorithm to take into account this
field of view constraint. Use the data in file data2.txt to test the algorithm. The columns of this file
are t, x, y, ✓, v, !, r1, 1, r2, 2.

## Task 3
Now assume that besides the field of view constraint the laser system only returns the range/bearing to
the closest landmark, not proving information to which lankmark the measrument refers . Update to
algorithm to deal with such feature. 

## Task 4
Consider now that there are several beacons in the operation are, that the laser system still has the above
field of view constraint and that it only returns the information to the closest landmark. Also assume
that you have no prior information about the initial robot pose.
Design and implement an algorithm (based on the SLAM concept) to estimate the position of the robot
and the lcoations of the landmarks in the operation area.
Now assume that besides the field of view constraint the laser system only returns the range/bearing to
the closest landmark, not proving information to which lankmark the measrument refers . Update to
algorithm to deal with such feature. Use the data in file data4.txt to test the algorithm.